"use strict";

// https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webRequest/onBeforeSendHeaders
browser.webRequest.onBeforeSendHeaders.addListener(
    function (e) {        
        for (var header of e.requestHeaders) {
            if (header.name.toLowerCase() === "host"
                && /^dyn\.in\./.test(header.value)) {
                var hostParts = header.value.replace(/^dyn\.in\./, '').split('.');

                if (hostParts.length <= 2
                    || /^(dev|staging|pp|prod)$/.test(hostParts[0])) {
                    hostParts.unshift('www');
                }

                header.value = hostParts.join('.');
            }
        }
        return {
            requestHeaders : e.requestHeaders
        };
    },
    {
        urls: [
            '<all_urls>'
        ],
        types: [
            'main_frame'
        ]
    },
    [
        "blocking", // Make it "blocking" so we can modify the headers.
        "requestHeaders"
    ]
);
